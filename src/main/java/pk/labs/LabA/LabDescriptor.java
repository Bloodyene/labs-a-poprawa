package pk.labs.LabA;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabA.Contracts.impl.DisplayBean";
    public static String controlPanelImplClassName = "pk.labs.LabA.Contracts.impl.ControlBean";

    public static String mainComponentSpecClassName = "pk.labs.LabA.Contracts.Main";
    public static String mainComponentImplClassName = "pk.labs.LabA.Contracts.impl.MainBean";
    // endregion

    // region P2
    public static String mainComponentBeanName = "main";
    public static String mainFrameBeanName = "mainFrame";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "silly";
    // endregion
}
